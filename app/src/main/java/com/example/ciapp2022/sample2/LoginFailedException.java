package com.example.ciapp2022.sample2;

public class LoginFailedException extends Exception {
    public LoginFailedException(String msg){
        super(msg);
    }
}
